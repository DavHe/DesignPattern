﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    //訓練營介面
    public interface ITrainingCamp
    {
        //訓練冒險者的過程，訓練後請給我一個冒險者
        IAdventurer TrainAdventurer();
    }

    public class ArcherTrainingCamp : ITrainingCamp
    {
        IAdventurer ITrainingCamp.TrainAdventurer()
        {
            Console.WriteLine("訓練弓箭手");
            return new Archer();
        }
    }

    public class WarriorTrainingCamp : ITrainingCamp
    {
        IAdventurer ITrainingCamp.TrainAdventurer()
        {
            Console.WriteLine("訓練鬥士");
            return new Warrior();
        }
    }
}

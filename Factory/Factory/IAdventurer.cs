﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    //冒險者介面
    public interface IAdventurer
    {
        //告訴別人你是哪種冒險者
        string GetType();
    }

    public class Archer : IAdventurer
    {
        string IAdventurer.GetType()
        {
            Console.WriteLine("我是弓箭手");
            return GetType().ToString();
        }
    }

    public class Warrior : IAdventurer
    {
        string IAdventurer.GetType()
        {
            Console.WriteLine("我是鬥士");
            return GetType().ToString();
        }
    }
}

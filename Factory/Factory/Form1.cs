﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Factory
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ITrainingCamp trainingCamp = new ArcherTrainingCamp();
            IAdventurer playerA = trainingCamp.TrainAdventurer();
            trainingCamp = new WarriorTrainingCamp();
            IAdventurer playerB = trainingCamp.TrainAdventurer();

            playerA.GetType();
            playerB.GetType();
        }
    }
}

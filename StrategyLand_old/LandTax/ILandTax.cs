﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTax
{
    enum LandType
    {
        normal,
        self,
        industry,
        publicLand
    }

    interface ILandTax
    {
        double CountTax(int landPrice);
    }
}

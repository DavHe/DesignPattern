﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTax
{
    interface INormalLandTax : ILandTax
    {
        int MinPrice { get; set; }
        int MaxPrice { get; set; }
    }
}

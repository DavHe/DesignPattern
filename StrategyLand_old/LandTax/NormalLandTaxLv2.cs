﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTax
{
    public class NormalLandTaxLv2 : INormalLandTax
    {
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public NormalLandTaxLv2()
        {
            MinPrice = 100001;
            MaxPrice = 200000;
        }

        public double CountTax(int landPrice)
        {
            return landPrice * 0.04 - 3214;
        }
    }
}

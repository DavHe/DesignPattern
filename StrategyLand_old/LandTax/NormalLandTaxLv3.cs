﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTax
{
    public class NormalLandTaxLv3 : INormalLandTax
    {
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public NormalLandTaxLv3()
        {
            MinPrice = 200001;
            MaxPrice = int.MaxValue;
        }

        public double CountTax(int landPrice)
        {
            return landPrice * 0.08 - 5643;
        }
    }
}

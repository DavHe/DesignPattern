﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTax
{
    public class PublicLandTax : ILandTax
    {
        public double CountTax(int landPrice)
        {
            return landPrice * 0.04;
        }
    }
}

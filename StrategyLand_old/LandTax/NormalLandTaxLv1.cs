﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTax
{
    public class NormalLandTaxLv1 : INormalLandTax
    {
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public NormalLandTaxLv1()
        {
            MinPrice = 0;
            MaxPrice = 100000;
        }

        public double CountTax(int landPrice)
        {
            return landPrice * 0.02;
        }
    }
}

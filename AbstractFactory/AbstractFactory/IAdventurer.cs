﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    //冒險者介面
    public abstract class Adventurer
    {
        protected Weapon weapon;//武器
        protected Clothes clothes; //衣服

        public abstract void Display();

        public void SetWeapon(Weapon wep)
        {
            weapon = wep;
        }

        public void SetClothes(Clothes cloth)
        {
            clothes = cloth;
        }
    }

    public class Archer : Adventurer
    {
        public override void Display()
        {
            Console.WriteLine("我是弓箭手");
            weapon.Display();
            clothes.Display();
        }
    }

    public class Warrior : Adventurer
    {
        public override void Display()
        {
            Console.WriteLine("我是鬥士");
            weapon.Display();
            clothes.Display();
        }
    }
}

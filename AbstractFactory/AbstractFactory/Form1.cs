﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AbstractFactory
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ITrainingCamp camp = new ArcherTrainingCamp();
            Adventurer archer = camp.TrainAdventurer();
            camp = new WarriorTrainingCamp();
            Adventurer worrior = camp.TrainAdventurer();
            archer.Display();
            worrior.Display();
        }
    }
}

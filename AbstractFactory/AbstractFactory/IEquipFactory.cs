﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    public interface IEquipFactory
    {
        Weapon MakeWeapon();

        Clothes MakeClothes();
    }

    public class ArcherEquipFactory : IEquipFactory
    {
        public Clothes MakeClothes()
        {
            Clothes cloth = new Leather();
            cloth.SetDef(2);
            return cloth;
        }

        public Weapon MakeWeapon()
        {
            Weapon weapon = new Bow();
            weapon.SetAtk(50);
            weapon.SetRange(200);
            return weapon;
        }
    }

    public class WarriorEquipFactory : IEquipFactory
    {
        public Clothes MakeClothes()
        {
            Clothes cloth = new Armor();
            cloth.SetDef(10);
            return cloth;
        }

        public Weapon MakeWeapon()
        {
            Weapon weapon = new LongSword();
            weapon.SetAtk(30);
            weapon.SetRange(26);
            return weapon;
        }
    }
}

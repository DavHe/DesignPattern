﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    public abstract class Clothes
    {
        protected int Def { get; set; }

        public void SetDef(int num)
        {
            Def = num;
        }

        public void Display()
        {
            Console.WriteLine($"def : {Def}");
        }
    }

    //鬥士衣服
    public class Armor : Clothes
    {
        
    }

    //弓箭手衣服
    public class Leather : Clothes
    {

    }

    public abstract class Weapon
    {
        protected int Atk { get; set; }
        protected int Range { get; set; }

        public void SetAtk(int num)
        {
            Atk = num;
        }

        public void SetRange(int num)
        {
            Range = num;
        }

        public void Display()
        {
            Console.WriteLine($"atk : {Atk}, Range : {Range}");
        }
    }

    //長劍
    public class LongSword : Weapon
    {

    }

    //弓
    public class Bow : Weapon
    {

    }
}

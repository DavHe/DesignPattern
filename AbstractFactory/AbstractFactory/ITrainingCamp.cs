﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactory
{
    //訓練營介面
    public interface ITrainingCamp
    {
        //訓練冒險者的過程，訓練後請給我一個冒險者
        Adventurer TrainAdventurer();
    }

    public class ArcherTrainingCamp : ITrainingCamp
    {
        Adventurer ITrainingCamp.TrainAdventurer()
        {
            Console.WriteLine("訓練弓箭手");
            IEquipFactory factory = new ArcherEquipFactory();
            Adventurer archer = new Archer();
            archer.SetWeapon(factory.MakeWeapon());
            archer.SetClothes(factory.MakeClothes());
            return archer;
        }
    }

    public class WarriorTrainingCamp : ITrainingCamp
    {
        Adventurer ITrainingCamp.TrainAdventurer()
        {
            Console.WriteLine("訓練鬥士");
            IEquipFactory factory = new WarriorEquipFactory();
            Adventurer warrior = new Warrior();
            warrior.SetWeapon(factory.MakeWeapon());
            warrior.SetClothes(factory.MakeClothes());
            return warrior;
        }
    }
}

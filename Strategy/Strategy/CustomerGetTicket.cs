﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class CustomerGetTicket
    {
        ITicketNrDiscountService _ticketNrDiscountService;

        public CustomerGetTicket()
        {
            _ticketNrDiscountService = new TicketNrDiscountService();
        }

        public CustomerGetTicket(ITicketNrDiscountService ticketNrDiscountService)
        {
            _ticketNrDiscountService = ticketNrDiscountService;
        }

        public int GetTicketSales(Customer customer, int ticketSales)
        {
            return _ticketNrDiscountService.CountDiscount(customer.Age, ticketSales);
        }
    }
}

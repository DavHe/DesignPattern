﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public abstract class TicketDiscount
    {
        public abstract int MinAge { get; }
        public abstract int MaxAge { get; }
        public abstract int Discount(int ticketSales);
    }

    public class OldDiscount : TicketDiscount
    {
        public override int MinAge =>65;
        public override int MaxAge => int.MaxValue;

        public override int Discount(int ticketSales)
        {
            return ticketSales /2;
        }
    }

    public class ChildrenDiscount : TicketDiscount
    {
        public override int MinAge => 0;
        public override int MaxAge => 8;

        public override int Discount(int ticketSales)
        {
            return ticketSales / 10 * 9;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strategy
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //範例一
            int amount = 0;
            amount = 1000000000;

            IDiscountService discountService = new DiscountService();

            discountService.CountDiscount(amount);

            //範例二
            int ticketSales = 200;
            Customer old1 = new Customer
            {
                Age = 67
            };

            Customer child1 = new Customer
            {
                Age = 3
            };

            CustomerGetTicket getTicket = new CustomerGetTicket();

            Console.WriteLine(getTicket.GetTicketSales(old1, ticketSales)) ;
            Console.WriteLine(getTicket.GetTicketSales(child1, ticketSales));
        }
    }
}

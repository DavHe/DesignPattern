﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public class Customer
    {
        public int Age { get; set; }
        public bool IsMale { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public abstract class Discount
    {
        //折扣的範圍
        public abstract int MinMount { get; }
        //折扣的範圍
        public abstract int MaxMount { get; }
        //實際折扣的計算
        public abstract void ShowDiscount();
    }

    public class MoneyDiscount : Discount
    {
        public override int MinMount => 10000;

        public override int MaxMount => int.MaxValue;

        public override void ShowDiscount()
        {
            Console.WriteLine("現金折扣");
        }
    }

    public class BonusDiscount : Discount
    {
        public override int MinMount => 1000;

        public override int MaxMount => 10000;

        public override void ShowDiscount()
        {
            Console.WriteLine("紅利折扣");
        }
    }

    public class PointDiscount : Discount
    {
        public override int MinMount => 0;

        public override int MaxMount => 1000;

        public override void ShowDiscount()
        {
            Console.WriteLine("點數折扣");
        }
    }
}

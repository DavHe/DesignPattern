﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace Strategy
{
    public interface ITicketNrDiscountService
    {
        int CountDiscount(int age, int amount);
    }

    public class TicketNrDiscountService : ITicketNrDiscountService
    {
        private List<TicketDiscount> _ticketDiscount;

        public TicketNrDiscountService()
        {
            this._ticketDiscount = new List<TicketDiscount>()
            {
                new OldDiscount(),
                new ChildrenDiscount()
            };
        }

        public int CountDiscount(int age, int amount)
        {
            return _ticketDiscount.FirstOrDefault(x => age <= x.MaxAge && age >= x.MinAge).Discount(amount);
        }
    }
}

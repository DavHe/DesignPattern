﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    public interface IDiscountService
    {
        void CountDiscount(int amount);
    }

    public class DiscountService : IDiscountService
    {
        List<Discount> _discountMap;

        public DiscountService()
        {
            _discountMap = new List<Discount>()
            {
                new MoneyDiscount(),
                new BonusDiscount(),
                new PointDiscount()
            };
        }

        public void CountDiscount(int amount)
        {
            _discountMap.FirstOrDefault(x => x.MaxMount >= amount && x.MinMount <= amount).ShowDiscount();
        }
    }
}

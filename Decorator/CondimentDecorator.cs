﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public abstract class CondimentDecorator : Beverage
    {
        //配料可能會修改相應的getDescription
        public abstract override string GetDescription();
        public abstract override double Cost();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public class Mocha : CondimentDecorator
    {
        private readonly Beverage MochaBeverage;

        public Mocha(Beverage beverage)
        {
            MochaBeverage = beverage;
        }

        public override string GetDescription()
        {
            return MochaBeverage.GetDescription() + ", Mocha";
        }
        public override double Cost()
        {
            return MochaBeverage.Cost() + 0.2;
        }
    }
}

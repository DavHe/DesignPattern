﻿using Decorator.Decorator2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    //參考網址:http://corrupt003-design-pattern.blogspot.com/2016/03/decorator-pattern.html
    //每個元件都可以單獨使用, 或是被裝飾者包起來使用
    class Program
    {
        static void Main(string[] args)
        {
            Beverage beverage1 = new Mocha(new Espresso());
            
            Console.WriteLine(beverage1.GetDescription());
            Console.WriteLine(beverage1.Cost());

            //Exp2
            //報價單系統, 計算運費
            //計算運費外, 有其他服務費
            var transport = new Transport
            {
                Miles = 200,
                Place = "台南",
                ExtraPlaceCnt = 1,
                IsHoliday = false,
                DelayHours = 3
            };

            IPricer stdPricer = new MilePricer()
            {
                Customer = "客戶1",
                Receiver = "收件人1",
                Freight = "貨物1"
            };

            IPricer extraPlacePricer = new ExtraPlacePricer(stdPricer);
            IPricer delayPricer = new DelayPricer(extraPlacePricer);

            delayPricer.Price(transport);

            Console.ReadLine();
        }
    }
}

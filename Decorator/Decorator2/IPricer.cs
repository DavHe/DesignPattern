﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Decorator2
{
    //原始的計算運費
    public interface IPricer
    {
        string Customer { get; set; }
        string Receiver { get; set; }
        string Freight { get; set; }

        decimal Price(Transport transport);
    }
}

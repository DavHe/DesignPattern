﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Decorator2
{
    //運送點計費
    public class PlacePricer : IPricer
    {
        public string Customer { get; set; }
        public string Receiver { get; set; }
        public string Freight { get; set; }

        public decimal Price(Transport transport)
        {
            //以運送點計算(如台南NTD$5,000、新竹NTD$1,000)
            var price = 0;
            switch (transport.Place)
            {
                case "台南":
                    price = 5000;
                    break;
                case "新竹":
                    price = 1000;
                    break;
                default:
                    price = 2500;
                    break;
            };

            Console.WriteLine($"以運送點計算 = {price}");
            return price;
        }
    }
}

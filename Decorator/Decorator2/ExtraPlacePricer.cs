﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Decorator2
{
    /// 加點服務計費
    public class ExtraPlacePricer : Decorator
    {
        public ExtraPlacePricer(IPricer pricer) : base(pricer)
        {
        }

        public override decimal Price(Transport transport)
        {
            decimal totalPrice = this.stdPricer.Price(transport);
            decimal servicePrice = 0;
            if (transport.ExtraPlaceCnt > 0)
            {
                servicePrice = totalPrice * (decimal)0.1;
                totalPrice = totalPrice + Math.Floor(servicePrice);
            }
            Console.WriteLine($"加點服務費用10% = {servicePrice}，總費用={totalPrice}");
            return totalPrice;

        }
    }
}

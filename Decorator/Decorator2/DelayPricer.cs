﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Decorator2
{
    /// 延遲計費
    public class DelayPricer : Decorator
    {
        public DelayPricer(IPricer pricer) : base(pricer)
        {
        }

        public override decimal Price(Transport transport)
        {
            var totalPrice = this.stdPricer.Price(transport);
            var servicePrice = transport.DelayHours * 500;
            totalPrice += (decimal)servicePrice;
            Console.WriteLine($"延遲費用:500/hr = {servicePrice}，總費用={totalPrice}");
            return totalPrice;

        }
    }
}

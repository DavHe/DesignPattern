﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Decorator2
{
    /// 假日運送服務計費
    public class HolidayPricer : Decorator
    {
        public HolidayPricer(IPricer pricer) : base(pricer)
        {
        }

        public override decimal Price(Transport transport)
        {
            var defaultPrice = this.stdPricer.Price(transport);
            var servicePrice = defaultPrice * (decimal)0.2;
            var totalPrice = defaultPrice + Math.Floor(servicePrice);
            Console.WriteLine($"假日運送服務費用20% = {servicePrice}，總費用={totalPrice}");
            return totalPrice;

        }
    }
}

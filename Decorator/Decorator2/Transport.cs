﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Decorator2
{
    //運送類別
    public class Transport
    {
        /// 運送點
        public string Place { get; set; }
        /// 里程
        public int Miles { get; set; }
        /// 加點數
        public int ExtraPlaceCnt { get; set; }
        /// 假日運送
        public bool IsHoliday { get; set; }
        /// 延誤時數
        public int DelayHours { get; set; } = 0;
    }
}

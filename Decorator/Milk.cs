﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public class Milk : CondimentDecorator
    {
        private readonly Beverage MilkBeverage;

        public Milk(Beverage beverage)
        {
            MilkBeverage = beverage;
        }

        public override string GetDescription()
        {
            return MilkBeverage.GetDescription() + ", Milk";
        }
        public override double Cost()
        {
            return MilkBeverage.Cost() + 0.3;
        }
    }
}

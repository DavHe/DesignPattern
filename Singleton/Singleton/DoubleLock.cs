﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Singleton
{
    public class DoubleLock
    {
        private static DoubleLock instance;
        private object lockObj = new object();

        private DoubleLock()
        { 
        
        }

        public DoubleLock GetInstance()
        {
            if (instance == null)
                lock (lockObj)
                    if (instance == null)
                        instance = new DoubleLock();

            return instance;
        }
    }
}

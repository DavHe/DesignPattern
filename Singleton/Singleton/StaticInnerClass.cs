﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Singleton
{
    public class StaticInnerClass
    {
        //很重要!!!!
        private StaticInnerClass()
        { 
        }

        private static class StaticInnerClassInstance
        {
            public static StaticInnerClass instance = new StaticInnerClass();
        }

        public StaticInnerClass GetInstance()
        {
            return StaticInnerClassInstance.instance;
        }
    }
}

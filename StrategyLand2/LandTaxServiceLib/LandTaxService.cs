﻿using System;
using System.Collections.Generic;
using StrategyLand.LandTaxLibrary;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxServiceLib
{
    public class LandTaxService : ILandTaxService
    {
        private HashSet<LandTax> LandTaxSet { get; set; }

        public LandTaxService()
        {
            LandTaxSet = new HashSet<LandTax>()
            {
                new NormalLandTax(new NormalLandTaxService()),
                new IndustryLandTax(),
                new SelfLandTax(),
                new PublicLandTax(),
            };
        }

        public double CountTax(int landPrice, LandUse use)
        {
            LandTax landTaxes = LandTaxSet.Where((landTax) => landTax.GetUse() == use ).ToArray()[0];

            return landTaxes.CountTax(landPrice);
        }
    }
}

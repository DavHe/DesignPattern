﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxServiceLib
{
    public interface INormalLandTaxService
    {
        double CountTax(int landPrice);
    }
}

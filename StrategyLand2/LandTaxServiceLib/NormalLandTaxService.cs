﻿using StrategyLand.LandTaxLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxServiceLib
{
    public class NormalLandTaxService : INormalLandTaxService
    {
        private List<NormalLandTaxSub> NormalLandTaxSet { get;set;}

        public NormalLandTaxService()
        {
            NormalLandTaxSet = new List<NormalLandTaxSub>()
            {
                new NormalLandTaxLv1(),
                new NormalLandTaxLv2(),
                new NormalLandTaxLv3()
            };
        }

        public double CountTax(int landPrice)
        {
            NormalLandTaxSub taxLv = NormalLandTaxSet.Where((x) => landPrice >= x.GetMinPrice() && landPrice <= x.GetMaxPrice()).ToArray()[0];

            return taxLv.CountTax(landPrice);
        }
    }
}

﻿using StrategyLand.LandTaxLibrary;
using StrategyLand.LandTaxServiceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand
{
    class Program
    {
        static void Main(string[] args)
        {
            ILandTaxService landTaxService = new LandTaxService();
            int[] ary = new int[] { 999, 100000, 100001, 300000,200000, 3600000};
            LandUse[] types = new LandUse[] { LandUse.normal, LandUse.industry, LandUse.publicLand, LandUse.self};

            //foreach (LandUse type in types)
            //{
            //    Console.WriteLine(landTaxService.CountTax(1000, type));
            //}

            foreach (int num in ary)
            {
                Console.WriteLine(landTaxService.CountTax(num, LandUse.normal));
            }

            Console.ReadLine();
        }
    }
}

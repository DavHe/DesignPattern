﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class NormalLandTaxLv2 : NormalLandTaxSub
    {
        protected override int MinPrice { get; set; }
        protected override int MaxPrice { get; set; }
        public NormalLandTaxLv2()
        {
            MinPrice = 100001;
            MaxPrice = 200000;
        }
        public override double CountTax(int landPrice)
        {
            return landPrice * 0.04 - 3215;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public enum LandUse
    {
        normal,
        self,
        industry,
        publicLand
    }

    public abstract class LandTax
    {
        protected abstract LandUse Use { get; set; }
        public abstract double CountTax(int landPrice);

        public LandUse GetUse()
        {
            return Use;
        }
    }
}

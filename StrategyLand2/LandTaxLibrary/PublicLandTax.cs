﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class PublicLandTax : LandTax
    {
        protected override LandUse Use { get; set; }

        public PublicLandTax()
        {
            Use = LandUse.publicLand;
        }

        public override double CountTax(int landPrice)
        {
            return landPrice * 0.04;
        }
    }
}

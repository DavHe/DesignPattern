﻿using StrategyLand.LandTaxServiceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class NormalLandTax : LandTax
    {
        protected override LandUse Use { get ; set ; }
        private INormalLandTaxService NormalService { get; set; }

        public NormalLandTax(INormalLandTaxService normalService)
        {
            Use = LandUse.normal;
            NormalService = normalService;
        }

        public override double CountTax(int landPrice)
        {
            return NormalService.CountTax(landPrice);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public abstract class NormalLandTaxSub
    {
        protected abstract int MinPrice { get; set; }
        protected abstract int MaxPrice { get; set; }
        public int GetMinPrice()
        {
            return MinPrice;
        }
        public int GetMaxPrice()
        {
            return MaxPrice;
        }
        public abstract double CountTax(int landPrice);        
    }
}

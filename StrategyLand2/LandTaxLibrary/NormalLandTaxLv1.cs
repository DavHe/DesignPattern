﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class NormalLandTaxLv1:NormalLandTaxSub
    {
        protected override int MinPrice { get; set; }
        protected override int MaxPrice { get; set; }
        public NormalLandTaxLv1()
        {
            MinPrice = 0;
            MaxPrice = 100000;
        }
        public override double CountTax(int landPrice)
        {
            return landPrice * 0.02;
        }
    }
}

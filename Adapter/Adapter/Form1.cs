﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adapter
{
    public partial class Form1 : Form
    {
        public struct testStruct
        {

            public void showText()
            {
                Console.WriteLine();
            }
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //案例:企鵝生病了, 所以請鴨子來代班

            //先看看一隻正版的企鵝
            KingPenguins penguin = new KingPenguins();
            MallardDuck duck = new MallardDuck();     //要充數成企鵝的鴨子！
                
            IPenguins duckAdapter = new DuckAdapter(duck);              //叫鴨子穿上企鵝套裝(轉接器)
            Console.Write("\n\n鴨子展示：\n");                                                 //先看看正版鴨子
            duck.Duck_quack();
            duck.Duck_walk();
              
            //我們以下建立了一個企鵝展示台
            Console.WriteLine("\n\n國王企鵝展示：");
            penguin.Penguin_gobble();
            penguin.Penguin_walk();
              
            Console.WriteLine("\n\n假裝是企鵝的鴨子展示：");
            duckAdapter.Penguin_gobble();
            duckAdapter.Penguin_walk();
        }
    }
}

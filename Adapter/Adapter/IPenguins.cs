﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    //我們的介面
    interface IPenguins
    {
        void Penguin_gobble();      //咯咯叫
        void Penguin_walk();
    }

   public class KingPenguins : IPenguins   //國王企鵝
   {
        public void Penguin_gobble()
        {
            Console.WriteLine("企鵝叫");
        }
     
        public void Penguin_walk()
        {
           Console.WriteLine("企鵝走路"); 
        }
    }
}

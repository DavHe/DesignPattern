﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    interface IDuck
    {
        void Duck_quack();
        void Duck_walk();
    }

    public class MallardDuck : IDuck
    {
        public void Duck_quack()
        {
            Console.WriteLine("鴨子叫");
        }

        public void Duck_walk()
        {
            Console.WriteLine("鴨子走路");
        }
    }
}

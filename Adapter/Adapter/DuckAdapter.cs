﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class DuckAdapter : IPenguins
    {
        private readonly IDuck Duck;

        public DuckAdapter(IDuck duck)
        {
            this.Duck = duck;
        }

        public void Penguin_gobble()
        {
            this.Duck.Duck_quack();
            Console.WriteLine("壓低聲音");
        }

        public void Penguin_walk()
        {
            this.Duck.Duck_walk();
            Console.WriteLine("翅膀向下伸值，左右搖擺！");
        }
    }
}

﻿using System;

namespace Prototype_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            SomeObject a = new SomeObject("no.1", 1);
            SomeObject b = (SomeObject)a.Clone();
            Console.WriteLine(a.Name);
            Console.WriteLine(b.Name);
        }
    }
}

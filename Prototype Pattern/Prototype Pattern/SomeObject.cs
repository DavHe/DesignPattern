﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype_Pattern
{
    public class SomeObject : ICloneable
    {
        public string Name { get; set; }
        public int Number { get; set; }

        public SomeObject(string name, int num)
        {
            Name = name;
            Number = num;
        }

        public object Clone()
        {
            SomeObject cloneObj = new SomeObject(Name, Number);
            return cloneObj;
        }
    }
}

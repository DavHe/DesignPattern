﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vistor_Pattern
{
    public class CarElementDoVisitor : ICarElementVisitor
    {
        public void Visit(Body body)
        {
            Console.WriteLine("Moving my body");
        }

        public void Visit(Car car)
        {
            Console.WriteLine("Starting my car");
        }

        public void Visit(Wheel wheel)
        {
            Console.WriteLine("Kicking my " + wheel.getName() + " wheel");
        }

        public void Visit(Engine engine)
        {
            Console.WriteLine("Starting my engine");
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vistor_Pattern
{
    public interface ICarElement
    {
        void Accept(ICarElementVisitor visitor);
    }
}

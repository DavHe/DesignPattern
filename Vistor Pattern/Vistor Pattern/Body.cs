﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vistor_Pattern
{
    // Body，Car，Engine，Wheel 是我們實際要操作的元件，
    // 也就是訪問者會訪問的元件
    public class Body : ICarElement
    {
        // accept 這個方法是實作自 CarElement 這個介面的，
        // 因此是在程式執行期決定要呼叫哪個元件的 accept，
        // 這也是第一次 method dispatch。另外每一個子 Visitor
        // 也都有實作 Visitor 介面，因此 visit 也是在程式執行期
        // 才決定要呼叫哪個 Visitor 的 visit，這是第二次 method dispatch

        // visit 代入 this 做為參數，這看起來很像多型，
        // 其實是 function overloading。
        // 另外傳入的 this 是編譯時期就決定好要傳入的是哪個 Element，
        // 因為以 Body 來說，在編譯時期就能決定好 this 為 Body 實體
        public void Accept(ICarElementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vistor_Pattern
{
    public class Wheel : ICarElement
    {
        private readonly string name;

        public Wheel(string name)
        {
            this.name = name;
        }

        public String getName()
        {
            return this.name;
        }

        public void Accept(ICarElementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vistor_Pattern
{
    // Visitor 介面。因為我們有四個要訪問的元件，
    // 所以有四個方法
    public interface ICarElementVisitor
    {
        void Visit(Body body);
        void Visit(Car car);
        void Visit(Engine engine);
        void Visit(Wheel wheel);
    }
}

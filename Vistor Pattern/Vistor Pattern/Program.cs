﻿using System;

namespace Vistor_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ICarElement car = new Car();

            // 基本上會依據不同用途而建立不同的具象 Visitor，
            // 因此同一個 car 會因為不同的 Visitor 而有
            // 不同執行結果
            car.Accept(new CarElementPrintVisitor());
            car.Accept(new CarElementDoVisitor());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vistor_Pattern
{
    public class CarElementPrintVisitor: ICarElementVisitor
    {
    public void Visit(Body body)
    {
        Console.WriteLine("Visiting body");
    }

    public void Visit(Car car)
    {
        Console.WriteLine("Visiting car");
    }

    public void Visit(Engine engine)
    {
        Console.WriteLine("Visiting engine");
    }

    public void Visit(Wheel wheel)
    {
        Console.WriteLine("Visiting " +
            wheel.getName() + " wheel");
    }
}

}

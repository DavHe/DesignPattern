﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vistor_Pattern
{
    public class Engine : ICarElement
    {
        public void Accept(ICarElementVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}

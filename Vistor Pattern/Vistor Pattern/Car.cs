﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vistor_Pattern
{
    public class Car : ICarElement
    {
        private ICarElement[] mElements;

        public Car()
        {
            this.mElements = new ICarElement[] {
            new Wheel("front left"), new Wheel("front right"),
            new Wheel("back left"), new Wheel("back right"),
            new Body(), new Engine() };
        }

        public void Accept(ICarElementVisitor visitor)
        {
            foreach (ICarElement elem in mElements)
            {
                elem.Accept(visitor);
            }
            visitor.Visit(this);
        }
    }
}

﻿using System;

namespace Memento_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            // 創造一個遊戲角色
            GamePlayer player = new GamePlayer(100, 0);

            // 先存個檔
            GameCaretaker caretaker = new GameCaretaker();
            caretaker.setMemento(player.saveToMemento());

            // 不小心死掉啦
            player.play(-100, 10);

            // 重新讀取存檔，又是一尾活龍
            player.restoreFromMemento(caretaker.getMemento());
        }
    }
}

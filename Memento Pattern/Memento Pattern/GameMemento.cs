﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Memento_Pattern
{
    public class GameMemento
    {
        // 假設只有這兩個資料要保留
        private readonly int mGameHp;
        private readonly int mGameExp;

        public GameMemento(int hp, int exp)
        {
            mGameHp = hp;
            mGameExp = exp;
        }

        public int getGameHp()
        {
            return mGameHp;
        }

        public int getGameExp()
        {
            return mGameExp;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Memento_Pattern
{
    // Originator
    public class GamePlayer
    {

        // 遊戲角色的生命值
        private int mHp;

        // 遊戲角色的經驗值
        private int mExp;

        public GamePlayer(int hp, int exp)
        {
            mHp = hp;
            mExp = exp;
        }

        public GameMemento saveToMemento()
        {
            return new GameMemento(mHp, mExp);
        }

        public void restoreFromMemento(GameMemento memento)
        {
            mHp = memento.getGameHp();
            mExp = memento.getGameExp();
        }

        public void play(int hp, int exp)
        {
            mHp = mHp - hp;
            mExp = mExp + exp;
        }
    }
}

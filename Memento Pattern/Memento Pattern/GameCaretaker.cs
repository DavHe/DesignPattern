﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Memento_Pattern
{
    public class GameCaretaker
    {
        // 保留要處理的資料。
        // 這邊只是範例，所以 Caretaker
        // 只能處理一個 Memento。
        // 實務上當然可以用更複雜的結構來
        // 處理多個 Memento，如 ArrayList。
        private GameMemento mMemento;

        public GameMemento getMemento()
        {
            return mMemento;
        }

        public void setMemento(GameMemento memento)
        {
            mMemento = memento;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bridge_Pattern
{
    public class ConcreteRemote : IRemoteControl
    {
        private ITvFucntion MyTv;

        public ConcreteRemote(ITvFucntion tv)
        {
            MyTv = tv;
        }

        public string getTvName()
        {
            return MyTv.getName();
        }

        public void off()
        {
            MyTv.off();
        }

        public void on()
        {
            MyTv.on();
        }

        public void setChannel(int channel)
        {
            MyTv.setChannel(channel);
        }
    }
}

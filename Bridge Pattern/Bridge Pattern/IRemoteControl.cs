﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bridge_Pattern
{
    public interface IRemoteControl
    {
        public void on();
        public void off();
        public void setChannel(int channel);
        public string getTvName();
    }
}

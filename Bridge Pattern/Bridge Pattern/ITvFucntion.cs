﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bridge_Pattern
{
    public interface ITvFucntion
    {
        // 這些是跟廠商高度相依的行為
        // 把這些行為封裝起來
        public void on();
        public void off();
        public void setChannel(int channel);
        public string getName();
    }
}

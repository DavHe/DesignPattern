﻿using System;

namespace Bridge_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ITvFucntion newTv = new SonyTv();
            IRemoteControl control = new ConcreteRemote(newTv);

            Console.WriteLine(control.getTvName());
            control.on();
            control.off();
            control.setChannel(102);
            control.setChannel(64);
        }
    }
}

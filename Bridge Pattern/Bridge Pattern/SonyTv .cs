﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bridge_Pattern
{
    public class SonyTv : ITvFucntion
    {
        private readonly string name = "SonyTv";

        public void off()
        {
            Console.WriteLine("SonyTv Off");
        }

        public void on()
        {
            Console.WriteLine("SonyTv On");
        }

        public void setChannel(int channel)
        {
            Console.WriteLine("Set SonyTv in Channel " + channel);
        }

        public string getName()
        {
            return name;
        }

    }
}

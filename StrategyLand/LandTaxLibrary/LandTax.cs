﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    enum LandUse
    {
        normal,
        self,
        industry,
        publicLand
    }

    public abstract class LandTax
    {
        public abstract string Use { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public abstract double CountTax(int landPrice);

        public LandTax()
        {
            MinPrice = int.MinValue;
            MaxPrice = int.MaxValue;
        }
    }
}

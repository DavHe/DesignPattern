﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class NormalLandTaxLv2 : LandTax
    {
        public override string Use { get; set; }

        public NormalLandTaxLv2()
        {
            Use = LandUse.normal.ToString();
            MinPrice = 100001;
            MaxPrice = 200000;
        }

        public override double CountTax(int landPrice)
        {
            return landPrice * 0.04 - 3215;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class IndustryLandTax : LandTax
    {
        public override string Use { get ; set ; }

        public IndustryLandTax()
        {
            Use = LandUse.industry.ToString();
        }

        public override double CountTax(int landPrice)
        {
            return landPrice * 0.1;
        }
    }
}

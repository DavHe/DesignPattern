﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class PublicLandTax : LandTax
    {
        public override string Use { get; set; }

        public PublicLandTax()
        {
            Use = LandUse.publicLand.ToString();
        }

        public override double CountTax(int landPrice)
        {
            return landPrice * 0.04;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class NormalLandTaxLv1 : LandTax
    {
        public override string Use { get; set; }       

        public NormalLandTaxLv1()
        {
            Use = LandUse.normal.ToString();
            MinPrice = 0;
            MaxPrice = 100000;
        }

        public override double CountTax(int landPrice)
        {
            return landPrice * 0.02;
        }
    }
}

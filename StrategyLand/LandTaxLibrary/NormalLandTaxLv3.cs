﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class NormalLandTaxLv3 : LandTax
    {
        public override string Use { get; set; }

        public NormalLandTaxLv3()
        {
            Use = LandUse.normal.ToString();
            MinPrice = 200001;
            MaxPrice = int.MaxValue;
        }

        public override double CountTax(int landPrice)
        {
            return landPrice * 0.08 - 5643;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxLibrary
{
    public class SelfLandTax : LandTax
    {
        public override string Use { get; set; }

        public SelfLandTax()
        {
            Use = LandUse.self.ToString();
        }
        public override double CountTax(int landPrice)
        {
            return landPrice * 0.06;
        }
    }
}

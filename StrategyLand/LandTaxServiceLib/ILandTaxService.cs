﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxServiceLib
{
    interface ILandTaxService
    {
        double CountTax(int landPrice,string use);
    }
}

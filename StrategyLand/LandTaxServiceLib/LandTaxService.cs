﻿using System;
using System.Collections.Generic;
using StrategyLand.LandTaxLibrary;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand.LandTaxServiceLib
{
    public class LandTaxService : ILandTaxService
    {
        private HashSet<LandTax> LandTaxSet { get; set; }

        public LandTaxService()
        {
            LandTaxSet = new HashSet<LandTax>()
            {
                new NormalLandTaxLv1(),
                new NormalLandTaxLv2(),
                new NormalLandTaxLv3(),
                new IndustryLandTax(),
                new SelfLandTax(),
                new PublicLandTax(),
            };
        }

        public double CountTax(int landPrice, string use)
        {
            IQueryable<LandTax> landTaxes = LandTaxSet.Where((landTax) => landTax.Use == use ).AsQueryable();
            landTaxes = landTaxes.Where((x)=> landPrice >= x.MinPrice && landPrice <= x.MaxPrice);

            return landTaxes.ToArray()[0].CountTax(landPrice);
        }
    }
}

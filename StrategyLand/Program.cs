﻿using StrategyLand.LandTaxLibrary;
using StrategyLand.LandTaxServiceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyLand
{
    class Program
    {
        static void Main(string[] args)
        {
            ILandTaxService landTaxService = new LandTaxService();
            int[] ary = new int[] { 999, 100000, 100001, 300000,200000, 3600000};
            

            foreach (int num in ary)
            {
                Console.WriteLine(landTaxService.CountTax(num, LandUse.publicLand.ToString()));
            }

            Console.ReadLine();
        }
    }
}

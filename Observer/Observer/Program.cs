﻿using System;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            ISubject newspaperOffice = new NewspaperOffice();
            newspaperOffice.RegisterObserver(new Customer("Jack"));
            newspaperOffice.RegisterObserver(new Customer("Tom"));
            newspaperOffice.RegisterObserver(new Customer("Peter"));

            newspaperOffice.NotifyObservers("newspaper1 is Sent...");

            newspaperOffice.RemoveObserver("Tom");
            newspaperOffice.RemoveObserver("People who is not exist");

            newspaperOffice.NotifyObservers("newspaper2 is Sent...");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    public class Customer : IObserver
    {
        private readonly string Name;

        public Customer(string name)
        {
            Name = name;
        }

        public string GetName()
        {
            return Name;
        }

        public void Update(string content)
        {
            Console.WriteLine(Name + " get newspaper: " + content);
        }
    }
}

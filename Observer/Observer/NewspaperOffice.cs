﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Observer
{
    public class NewspaperOffice : ISubject
    {
        private HashSet<IObserver> Observers;

        public NewspaperOffice()
        {
            Observers = new HashSet<IObserver>();
        }

        public void RegisterObserver(IObserver observer)
        {
            if(!Observers.Where((x) => x.GetName() == observer.GetName()).Any())
                Observers.Add(observer);
        }

        public void RemoveObserver(string observerName)
        {
            Observers.RemoveWhere((x) => x.GetName() == observerName);
        }

        public void NotifyObservers(string content)
        {
            foreach (IObserver observer in Observers)            
                observer.Update(content);            
        }
    }
}

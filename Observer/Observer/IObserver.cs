﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Observer
{
    public interface IObserver
    {
        public string GetName();
        public void Update(string content);
    }
}

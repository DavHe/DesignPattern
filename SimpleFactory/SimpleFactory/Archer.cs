﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFactory
{
    class Archer : IAdventurer
    {
        public Archer()
        {
            Console.WriteLine("我是弓箭手");
        }

        string IAdventurer.ShowType()
        {
            return "弓箭手";
        }
    }
}

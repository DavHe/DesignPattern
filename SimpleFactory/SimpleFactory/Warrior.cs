﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFactory
{
    class Warrior : IAdventurer
    {
        public Warrior()
        {
            Console.WriteLine("我是鬥士");
        }

        string IAdventurer.ShowType()
        {
            return "鬥士";
        }
    }
}
